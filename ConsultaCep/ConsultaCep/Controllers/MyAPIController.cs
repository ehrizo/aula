﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConsultaCep.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ConsultaCep.Controllers
{
    [Route("api/[controller]")]
    public class MyAPIController : Controller
    {
        // GET: api/<controller>
        [HttpGet]
        public JsonResult Get()
        {
            return Json(new Endereco()
            {
                Bairro = "Damha 2",
                Cep = "19053-761",
                Cidade = "Presidente Prudente",
                Complemento = "",
                Logradouro = "Rua Genovefa Angeli Ligabone",
                Numero = "89",
                UF = "SP"
            });
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public void Post(IFormCollection form)
        {
            var x = form["nome"];
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, IFormCollection form)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
