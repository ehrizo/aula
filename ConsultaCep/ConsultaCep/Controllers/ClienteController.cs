﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConsultaCep.Models;
using Microsoft.AspNetCore.Mvc;

namespace ConsultaCep.Controllers
{
    public class ClienteController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ConsultaCep(string cep)
        {
            try
            {
                wsCorreio.AtendeClienteClient correio =
                        new wsCorreio.AtendeClienteClient();
                wsCorreio.consultaCEPResponse dados =
                    await correio.consultaCEPAsync(cep);

                Endereco endereco = new Endereco()
                {
                    Logradouro = dados.@return.end,
                    Bairro = dados.@return.bairro,
                    Cep = dados.@return.cep,
                    Cidade = dados.@return.cidade,
                    Complemento = dados.@return.complemento2,
                    Numero = "",
                    UF = dados.@return.uf
                };

                return Json(endereco);
            }
            catch
            {
                return Json("");
            }
        }
    }
}